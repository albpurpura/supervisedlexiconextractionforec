import logging

import numpy as np
import scipy
from glmnet_py import cvglmnet, cvglmnetCoef, cvglmnetPredict
# from _shogun import PCA
from shogun import *
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC

import document_embeddings_creation as dec
import read_write as rw
import w2v

SIZE_W2V = 100
NEW_EMBS_SIZE = 50


def evaluate_w_k_fold_no_ml(documents_text, labels, print_res_to_file=True, fname='results_pipeline.csv'):
    labels = np.array(labels)
    n_folds = 5
    model_w2v = w2v.train_w2v_model(documents_text, size=SIZE_W2V, model_dest='./w2vmodel')
    headers = ['Method', 'ML', 'Classifier', 'Accuracy', 'AP', 'AR', 'AF1', 'Tot Correct', '# Correctly Predicted',
               '# Guesses']
    global_res = []
    params = [{'documents': documents_text, 'size': None, 'option': 'tfidf'},
              # {'documents': documents_text, 'size': SIZE_W2V, 'option': 'ftt', 'model': model_ftt},
              {'documents': documents_text, 'size': SIZE_W2V, 'option': 'w2v', 'model': model_w2v}]

    for p in params:
        print('* Document embeddings: ' + p['option'])
        docs_embeddings, feature_names = dec.get_document_representation(**p)
        docs_embeddings = np.array(docs_embeddings)
        # for reduce in ['mlr1', 'pca', 'none']:
        for reduce in ['none']:
            if p['option'] == 'w2v' and reduce != 'none':
                continue
            current_res = perform_tests(docs_embeddings, labels, n_folds, reduce, p)
            global_res.extend(current_res)
            print(current_res)
    if print_res_to_file:
        rw.print_table(global_res, out_path='./' + fname, headers=headers)
    else:
        return global_res


def run_comparison_w_fasttext():
    gt_file_path = './data/tweets/NEW_emotions_tweets_no_ids_my_format.csv'
    doc_file_path = gt_file_path

    # Read input data
    documents_gt = rw.read_tsv_file(doc_file_path, headers=True, encoding='utf-8')
    documents_text = [d[0] for d in documents_gt]
    labels = [d[1:] for d in documents_gt]
    for i in range(len(labels)):
        for j in range(len(labels[i])):
            labels[i][j] = int(labels[i][j])
    labels = np.array(labels)
    labels = np.array([float(np.argmax(e)) for e in labels], dtype=np.float64)
    # labels = labels
    p = {'documents': documents_text, 'size': None, 'option': 'tfidf'}
    docs_embeddings, feature_names = dec.get_document_representation(**p)
    docs_embeddings = np.array(docs_embeddings)
    x_train, x_test, y_train, y_test = train_test_split(docs_embeddings, labels, test_size=0.30, random_state=42)
    reduced_docs_embeddings = reduce_size_multinomial(x_train, y_train, docs_embeddings)
    x_train, x_test, y_train, y_test = train_test_split(reduced_docs_embeddings, labels, test_size=0.30,
                                                        random_state=42)
    classifier = LinearSVC(random_state=42, C=12.5)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test)
    print(str([accuracy, ap, ar, af1]))


def evaluate_classification_fasttext():
    y_test_path = 'data/ftt_test_TEC.txt'
    predicted_labels_path = 'data/ftt_predictions_TEC.txt'
    correct_labels = rw.read_tsv_file(y_test_path, delimiter=' ', headers=False)
    correct_labels = [c[0] for c in correct_labels]
    predicted_labels = rw.read_tsv_file(predicted_labels_path, delimiter=' ', headers=False)
    predicted_labels = [c[0] for c in predicted_labels]
    return evaluate_classification(predicted_labels, correct_labels)


def perform_tests(docs_embeddings, labels, n_folds, reduce, p):
    kf = StratifiedKFold(n_splits=n_folds, random_state=42)
    current_res = []
    avg_stats_per_fold = {'K-NN': np.zeros(7), 'MLP': np.zeros(7), 'SVM': np.zeros(7),
                          'RF': np.zeros(7), 'GNB': np.zeros(7), 'MLR': np.zeros(7)}
    docs_embeddings = np.array(docs_embeddings)
    labels = np.array([np.argmax(e) for e in labels])

    if reduce == 'pca':
        print('Dimensionality reduction with PCA')
        reduced_docs_embeddings = PCA(n_components=NEW_EMBS_SIZE).fit_transform(docs_embeddings)
    elif reduce == 'mlr1':
        print('Dimensionality reduction with MRL1')
        de_train, de_test, l_train, l_test = train_test_split(docs_embeddings, labels, stratify=labels, train_size=0.5)
        reduced_docs_embeddings = reduce_size_multinomial(de_train,
                                                          np.array([float(e) for e in l_train], dtype=np.float64),
                                                          docs_embeddings.copy())
        reduced_docs_embeddings = np.array(reduced_docs_embeddings)
    else:
        reduced_docs_embeddings = docs_embeddings.copy()

    for train_index, test_index in kf.split(reduced_docs_embeddings, labels):
        y_train, y_test = labels[train_index], labels[test_index]
        x_train, x_test = reduced_docs_embeddings[train_index], reduced_docs_embeddings[test_index]
        # res = evaluate_optimize_different_classifiers(x_train, x_test, y_train, y_test)
        res = classify_w_different_classifiers(x_train, x_test, y_train, y_test)
        for key, val in res.items():
            avg_stats_per_fold[key] = np.add(np.array(val), avg_stats_per_fold[key])
    for key, value in avg_stats_per_fold.items():
        avg_stats_per_fold[key] = np.divide(value, n_folds)
        curr_val = [p['option'], reduce, key]
        curr_val.extend(np.divide(value, n_folds))
        current_res.append(curr_val)
    return current_res


def classify_w_different_classifiers(x_train, x_test, y_train, y_test, avg='macro'):
    results = {}
    classifier = LogisticRegression(random_state=42)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg)
    results['MLR'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]
    print('accuracy=%2.5f, ap=%2.4f, ar=%2.4f, af1=%2.4f' % (accuracy, ap, ar, af1))

    # accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = mlr_classfier(x_train, y_train, x_test, y_test)
    # results['MLR'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]
    # print('accuracy=%2.5f, ap=%2.4f, ar=%2.4f, af1=%2.4f' % (accuracy, ap, ar, af1))
    #
    """
    classifier = LinearSVC(random_state=42, C=12.5)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg)
    results['SVM'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    classifier = KNeighborsClassifier(n_neighbors=2, n_jobs=-1)  # or 13
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg)
    results['K-NN'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    classifier = MLPClassifier(random_state=42, hidden_layer_sizes=2000, max_iter=500)  # 5, or 50 for w2v embeddings
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg)
    results['MLP'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    # for tfidf with bigrams
    # classifier = RandomForestClassifier(random_state=42, n_estimators=169, max_depth=48)
    # {'random_state': 42, 'n_estimators': 385, 'max_depth': 15} for tfidf with PCA, size 100
    # for w2v
    classifier = RandomForestClassifier(random_state=42, n_estimators=175, max_depth=41, n_jobs=-1)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg)
    results['RF'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    classifier = GaussianNB()
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg)
    results['GNB'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]
    """
    return results


def evaluate_classification(predicted_labels, y_test, avg='macro'):
    ################################################################
    # Evaluate classification                                      #
    # http://scikit-learn.org/stable/modules/model_evaluation.html #
    ################################################################
    accuracy = accuracy_score(y_true=y_test, y_pred=predicted_labels)
    cm = confusion_matrix(y_true=y_test, y_pred=predicted_labels)
    print(cm)
    print('Accuracy: ' + str(accuracy))
    logging.debug('Accuracy: ' + str(accuracy))
    logging.debug('Confusion matrix: ' + str(cm))
    logging.debug(classification_report(y_true=y_test, y_pred=predicted_labels))
    ap, ar, af1, support = precision_recall_fscore_support(y_true=y_test, y_pred=predicted_labels, average=avg)
    print('*** non averaged ***')
    ap_na, ar_na, af1_na, support_na = precision_recall_fscore_support(y_true=y_test, y_pred=predicted_labels)
    print(af1_na)
    # ap, ar, af1, support = precision_recall_fscore_support(y_true=y_test, y_pred=predicted_labels, average='macro')
    # print('WARNING: Binary classification option chosen for the evaluation!!!')
    # ap, ar, af1, support = precision_recall_fscore_support(y_true=y_test, y_pred=predicted_labels, average='binary')

    tot_correct = 0
    n_guesses = 0
    correct_guesses = 0
    for i in range(len(predicted_labels)):
        if predicted_labels[i] == y_test[i] and predicted_labels[i] == 1:
            correct_guesses += 1
        if predicted_labels[i] == 1:
            n_guesses += 1
        if y_test[i] == 1:
            tot_correct += 1
    return accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses


def mlr_classfier(x_train, y_train, x_test, y_test):
    y_train = np.array(y_train, dtype=np.float64)
    y_test = np.array(y_test, dtype=np.float64)
    cvfit = cvglmnet(x=x_train.copy(), y=y_train.copy(), family='multinomial', mtype='grouped', parallel=True)

    preds = cvglmnetPredict(cvfit, newx=x_test, s='lambda_min')
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(preds, y_test)
    return accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses


def reduce_size_multinomial(x, y, de):
    scipy.random.seed(0)
    document_embeddings = de.copy()
    cvfit = cvglmnet(x=x.copy(), y=y.copy(), family='multinomial', mtype='grouped', parallel=True)
    coeffs = cvglmnetCoef(cvfit, s='lambda_min')
    l = []
    for coeff_list in coeffs:
        coeff_list = coeff_list[1:]
        l.extend(np.nonzero(coeff_list)[0])
    l = list(set(l))
    if len(l) > 0:
        print('# total features: ' + str(len(document_embeddings[0])))
        document_embeddings = [e[l] for e in document_embeddings]
        print('Indices of survived features: ' + str(l))
        print('# survived features: ' + str(len(l)))
    return document_embeddings


def reduce_size_binomial(x, y, de):
    scipy.random.seed(0)
    document_embeddings = de.copy()
    cvfit = cvglmnet(x=x.copy(), y=y.copy(), family='binomial', parallel=True)
    coeffs = cvglmnetCoef(cvfit, s='lambda_min')[1:]
    assert len(coeffs) == len(x[0])  # the number of the coefficients is the same as an input item size
    l = np.nonzero(coeffs)[0]
    if len(l) > 0:
        print('# total features: ' + str(len(document_embeddings[0])))
        document_embeddings = [e[l] for e in document_embeddings]
        print('Indices of survived features: ' + str(l))
        print('# survived features: ' + str(len(l)))
    return document_embeddings


def evaluate_optimize_different_classifiers(x_train, x_test, y_train, y_test, avg='macro'):
    ##############
    # Classifier #
    ##############
    # Libreria Lime http://pythondata.com/local-interpretable-model-agnostic-explanations-lime-python/
    # http://marcotcr.github.io/lime/tutorials/Lime%20-%20basic%20usage%2C%20two%20class%20case.html

    # y_test = [np.argmax(e) for e in y_test]
    # y_train = [np.argmax(e) for e in y_train]
    results = {}
    logging.debug('K-NN classifier: ')
    classifier = KNeighborsClassifier()
    parameters = {'n_neighbors': range(1, 15)}
    clf = RandomizedSearchCV(classifier, parameters, n_jobs=-1)
    clf.fit(x_train, y_train)
    bp = clf.best_params_
    print(str(bp))
    logging.debug(bp)
    classifier = KNeighborsClassifier()
    classifier.set_params(**bp)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg=avg)
    results['K-NN'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    logging.debug('MLP classifier: ')
    classifier = MLPClassifier(random_state=42)
    parameters = {
        'hidden_layer_sizes': [5, 10, 50, (5, 5, 5, 5), (50, 50, 50), (500, 500), 1000, 2000, 3000, 3500, 4000, 5000],
        'random_state': [42], 'max_iter': [500]}
    clf = RandomizedSearchCV(classifier, parameters, n_jobs=-1)
    clf.fit(x_train, y_train)
    bp = clf.best_params_
    print(str(bp))
    logging.info(bp)
    classifier = MLPClassifier()
    classifier.set_params(**bp)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg=avg)
    results['MLP'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    logging.debug('SVM classifier: ')
    classifier = LinearSVC(random_state=42)
    parameters = {'C': np.arange(0.5, 50, 0.5), 'random_state': [42]}
    clf = RandomizedSearchCV(classifier, parameters, n_jobs=-1)
    clf.fit(x_train, y_train)
    bp = clf.best_params_
    print(str(bp))
    logging.debug(bp)
    classifier = LinearSVC()
    classifier.set_params(**bp)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg=avg)
    results['SVM'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    logging.debug('RF classifier: ')
    classifier = RandomForestClassifier(random_state=42)
    parameters = {'n_estimators': range(1, 500), 'random_state': [42], 'max_depth': range(1, 50)}
    clf = RandomizedSearchCV(classifier, parameters, n_jobs=-1)
    clf.fit(x_train, y_train)
    bp = clf.best_params_
    print(str(bp))
    logging.debug(bp)
    classifier = RandomForestClassifier()
    classifier.set_params(**bp)
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg=avg)
    results['RF'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]

    logging.debug('Gaussian NB classifier: ')
    classifier = GaussianNB()
    classifier.fit(x_train, y_train)
    predicted_labels = classifier.predict(x_test)
    accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses = evaluate_classification(predicted_labels, y_test,
                                                                                             avg=avg)
    results['GNB'] = [accuracy, ap, ar, af1, tot_correct, correct_guesses, n_guesses]
    return results


def run():
    # Input data parameters
    gt_file_path = './data/tweets/NEW_emotions_tweets_no_ids_my_format.csv'  # './data/tweets/gt2_sorted.csv'
    doc_file_path = gt_file_path

    # Read input data
    documents_gt = rw.read_tsv_file(doc_file_path, headers=True, encoding='utf-8')
    documents_text = [d[0] for d in documents_gt]
    labels = [d[1:] for d in documents_gt]
    for i in range(len(labels)):
        for j in range(len(labels[i])):
            labels[i][j] = int(labels[i][j])
    labels = np.array(labels)

    # Run the tests
    evaluate_w_k_fold_no_ml(documents_text, labels)


if __name__ == '__main__':
    # print(evaluate_classification_fasttext())
    run()
