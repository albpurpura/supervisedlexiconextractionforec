import nlp
from sklearn.feature_extraction.text import TfidfVectorizer

__author__ = 'Alberto Purpura'
__copyright__ = 'Copyright 2018, University of Padova'
__credits__ = ['Alberto Purpura', 'Chiara Masiero', 'Gian Antonio Susto']
"""
Copyright 2018 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


def get_tfidf_model(docs, vocabulary):
    vocabulary = set(vocabulary)
    tfidf_vectorizer = get_tfidf_vectorizer(vocabulary)
    tfidf_vectorizer.fit(docs)
    # print('Extracted features:')
    # print(tfidf_vectorizer.get_feature_names())
    # create model:
    tfidf_model = TFIDF_vectorize(docs, tfidf_vectorizer)
    return tfidf_model, tfidf_vectorizer


def get_tfidf_model_unsupervised(docs, max_features_num):
    print('TFIDF with 2-grams')
    tfidf_vectorizer = TfidfVectorizer(tokenizer=nlp.tokenize_quick, max_features=max_features_num, strip_accents=False,
                                       stop_words='english', ngram_range=(1, 2), min_df=5)
    tfidf_vectorizer.fit(docs)
    # create model:
    tfidf_model = TFIDF_vectorize(docs, tfidf_vectorizer)
    print(tfidf_vectorizer.get_feature_names())
    return tfidf_model, tfidf_vectorizer


def get_tfidf_matrix_features_list_unsupervised(docs, max_features_num, vocabulary=None):
    if vocabulary is not None:
        tfidf_model, tfidf_vectorizer = get_tfidf_model(docs, vocabulary)
    else:
        tfidf_model, tfidf_vectorizer = get_tfidf_model_unsupervised(docs, max_features_num)
    tf_idf_matrix = tfidf_model.todense()
    return tf_idf_matrix, tfidf_vectorizer.get_feature_names()


def get_tfidf_vectorizer(vocabulary):
    # Returns a TFIDF vectorizer object from sklearn library
    # the tokenizer is the function tokenize here in this file
    # tokenizer=nlp.tokenize, analyzer='word',
    tfidf_vectorizer = TfidfVectorizer(vocabulary=vocabulary)
    # ngram_range = (1, 2) means that I will consider also unigrams and bigrams in the training process
    return tfidf_vectorizer


# documents is a list of strings, each string is a document
def TFIDF_vectorize(documents, tfidf_vectorizer):
    """
    Does the actual computation of the frequency of the terms in the documents collection from the tfidf vectorizer object
    :param documents: documents to vectorize
    :param tfidf_vectorizer: the tfidf vectorizer object that contains all the instructions to perform the vectorization
    :return: the tfidf model of the documents
    """
    # print('Documents number: ' + str(len(documents)))
    tfidf = tfidf_vectorizer.transform(documents)
    return tfidf


def get_most_relevant_tfidf_model_terms(documents, n_terms_to_extract):
    tfidf_vectorizer = TfidfVectorizer(tokenizer=nlp.tokenize, max_features=n_terms_to_extract)
    tfidf_vectorizer.fit(documents)
    return tfidf_vectorizer.get_feature_names()


def extend_vocabulary_with_tfidf(documents, initial_vocab, n_terms_to_extract):
    """
    Extends the initial vocabulary using tf-idf to select the most representative terms among the input documents.
    :param documents: input documents to be used to select the most meaningful terms to add to the vocabulary.
    :param initial_vocab: a list of terms.
    :param n_terms_to_extract: the max number of terms to add to the initial_vocab
    :return: the initial vocab list exended with a certain number of terms.
    """
    relevant_terms_tfidf = get_most_relevant_tfidf_model_terms(documents, n_terms_to_extract)
    initial_vocab.extend(relevant_terms_tfidf)
    new_vocab = set(initial_vocab)
    new_vocab = nlp.remove_sw_shortwords_numbers_punct(new_vocab)
    return new_vocab
