import io
import csv
import os
import pickle

__author__ = 'Alberto Purpura'
__copyright__ = 'Copyright 2018, University of Padova'
__credits__ = ['Alberto Purpura', 'Chiara Masiero', 'Gian Antonio Susto']
"""
Copyright 2018 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


def print_dict_values(dict, out):
    index_entries = [(k, v) for k, v in dict.items()]
    o = open(out, 'w')
    for i in index_entries:
        o.write(str(i[0]) + '\t' + str(i[1]) + '\n')
    o.close()


def print_dict(dictionary, out_path, separator=',', headers=None):
    with open(out_path, 'w') as f:
        # print headers if present
        if headers is not None:
            to_print = separator.join([('\"' + str(x).replace('\"', '') + '\"') for x in headers]).strip(separator)
            f.write(to_print + '\n')
        for k, v in dictionary.items():
            to_print = '"' + str(k) + '"' + separator + '"' + str(v) + '"'
            f.write(to_print + '\n')
        f.close()


def print_table(table, out_path, separator=',', headers=None):
    with open(out_path, 'w') as f:
        # print headers if present
        if headers is not None:
            to_print = separator.join([('\"' + str(x).replace('\"', '') + '\"') for x in headers]).strip(separator)
            f.write(to_print + '\n')
        for line in table:
            to_print = separator.join(['\"' + str(x).replace('\"', '') + '\"' for x in line]).strip(separator)
            f.write(to_print + '\n')
        f.close()


def read_tsv_file(path, delimiter=',', headers=True, encoding='latin-1'):
    rows = []
    if headers:
        cnt = 0
    else:
        cnt = 1
    with io.open(path, 'r', encoding=encoding, errors='ignore') as f:
        for line in csv.reader((x.replace('\0', '') for x in f), delimiter=delimiter):
            # if len(line) < 1: continue
            if cnt > 0:
                rows.append(line)
            else:
                cnt += 1
    return rows


def save_model(model, output_path):
    with open(output_path, 'wb') as handle:
        pickle.dump(model, handle)
        handle.close()


def load_model(path):
    model = pickle.load(open(path, 'rb'))
    return model
