import scipy

from glmnet_py import cvglmnet, cvglmnetCoef
from sklearn.model_selection import StratifiedKFold, train_test_split
import read_write as rw
import numpy as np
import document_embeddings_creation as dec
import classify_headlines as ch
import App as a


def run():
    n_classes = 6
    features_by_class = get_feature_names_from_tweets()
    headlines1k = './data/1000Headlines/training data_1000headlines/training_data_my_format.csv'
    headlines250 = './data/1000Headlines/test data_250headlines/test_data_my_format.csv'
    x_test_text, y_test = ch.transform_input_data(headlines250)
    x_train_text, y_train = ch.transform_input_data(headlines1k)

    training_dataset_by_class = ch.get_multiple_data_sets_for_each_class_in_problem(x_train_text, y_train, n_classes)
    test_datasets_by_class = ch.get_multiple_data_sets_for_each_class_in_problem(x_test_text, y_test, n_classes)

    results_all_classes = []
    for c in range(n_classes):
        res_curr_class = []
        average_results_dict = {}
        documents_text = test_datasets_by_class[c]
        label = list(np.zeros(2))
        label[1] = 1
        labels = list([label] * len(documents_text))
        for oc in range(n_classes):
            if oc != c:
                oc_docs_text = test_datasets_by_class[oc]
                oc_label = list(np.zeros(2))
                oc_label[0] = 1
                for i in range(len(oc_docs_text)):
                    d = oc_docs_text[i]
                    if d not in documents_text:
                        documents_text.append(d)
                        labels.append(oc_label)
        labels = [np.argmax(e) for e in labels]
        labels = np.array(labels)
        docs_embeddings, feature_names = dec.get_document_representation(documents=documents_text, size=None,
                                                                         model=features_by_class[c], option='tfidf')

        documents_text = training_dataset_by_class[c]
        label = list(np.zeros(2))
        label[1] = 1
        labels_train = list([label] * len(documents_text))
        for oc in range(n_classes):
            if oc != c:
                oc_docs_text = training_dataset_by_class[oc]
                oc_label = list(np.zeros(2))
                oc_label[0] = 1
                for i in range(len(oc_docs_text)):
                    d = oc_docs_text[i]
                    if d not in documents_text:
                        documents_text.append(d)
                        labels_train.append(oc_label)
        labels_train = [np.argmax(e) for e in labels_train]
        labels_train = np.array(labels_train)

        training_de, training_fn = dec.get_document_representation(documents=documents_text, size=None,
                                                                         model=features_by_class[c], option='tfidf')
        training_de = np.array(training_de)
        docs_embeddings = np.array(docs_embeddings)

        results = a.classify_w_different_classifiers(training_de, docs_embeddings, labels_train, labels, avg='binary')
        # results = a.evaluate_optimize_different_classifiers(x_train, x_test, y_train, y_test)
        for k, v in results.items():
            if v[-1] != 0:
                prec = v[-2] / v[-1]
            else:
                prec = np.nan
            if v[-3] != 0:
                rec = v[-2] / v[-3]
            else:
                rec = np.nan
            v.append(prec)  # precision
            v.append(rec)  # recall
            if k not in average_results_dict.keys():
                average_results_dict[k] = np.add(np.zeros(9), np.array(v))
            else:
                average_results_dict[k] = np.add(np.array(v), average_results_dict[k])

        # average values across folds:
        for k, v in average_results_dict.items():
            average_results_dict[k] = list(v[4:7])
            average_results_dict[k].extend(v[7:] / 1)
        for r, data in average_results_dict.items():
            res_curr_class.append([c, 'tfidf', 'mlrl1', r])
            res_curr_class[len(res_curr_class) - 1].extend(data)
        results_all_classes.extend(res_curr_class)
    rw.print_table(results_all_classes, 'results_classification_250headlines_by_class_w_xvalid_dom_transfer.csv',
                   headers=['Class', 'Method', 'ML', 'Classifier', 'Tot Correct',
                            '# Correctly Predicted', '# Guesses', 'Mean Precision', 'Mean Recall'])


def run_w_kf():
    n_folds = 5
    kf = StratifiedKFold(n_splits=n_folds, random_state=42)
    n_classes = 6
    features_by_class = get_feature_names_from_tweets()
    headlines1k = './data/1000Headlines/training data_1000headlines/training_data_my_format.csv'
    headlines250 = './data/1000Headlines/test data_250headlines/test_data_my_format.csv'
    x_test_text, y_test = ch.transform_input_data(headlines250)
    x_train_text, y_train = ch.transform_input_data(headlines250)

    training_dataset_by_class = ch.get_multiple_data_sets_for_each_class_in_problem(x_train_text, y_train, n_classes)
    test_datasets_by_class = ch.get_multiple_data_sets_for_each_class_in_problem(x_test_text, y_test, n_classes)

    results_all_classes = []
    for c in range(n_classes):
        res_curr_class = []
        average_results_dict = {}
        documents_text = test_datasets_by_class[c]
        label = list(np.zeros(2))
        label[1] = 1
        labels = list([label] * len(documents_text))
        for oc in range(n_classes):
            if oc != c:
                oc_docs_text = test_datasets_by_class[oc]
                oc_label = list(np.zeros(2))
                oc_label[0] = 1
                for i in range(len(oc_docs_text)):
                    d = oc_docs_text[i]
                    if d not in documents_text:
                        documents_text.append(d)
                        labels.append(oc_label)
        labels = [np.argmax(e) for e in labels]
        labels = np.array(labels)
        docs_embeddings, feature_names = dec.get_document_representation(documents=documents_text, size=None,
                                                                         model=features_by_class[c], option='tfidf')

        documents_text = training_dataset_by_class[c]
        label = list(np.zeros(2))
        label[1] = 1
        labels_train = list([label] * len(documents_text))
        for oc in range(n_classes):
            if oc != c:
                oc_docs_text = training_dataset_by_class[oc]
                oc_label = list(np.zeros(2))
                oc_label[0] = 1
                for i in range(len(oc_docs_text)):
                    d = oc_docs_text[i]
                    if d not in documents_text:
                        documents_text.append(d)
                        labels_train.append(oc_label)
        labels_train = [np.argmax(e) for e in labels_train]
        labels_train = np.array(labels_train)

        training_de, training_fn = dec.get_document_representation(documents=documents_text, size=None,
                                                                         model=features_by_class[c], option='tfidf')
        training_de = np.array(training_de)
        docs_embeddings = np.array(docs_embeddings)

        for train_index, test_index in kf.split(training_de, labels_train):
            x_train, x_test = training_de[train_index], training_de[test_index]
            y_train, y_test = labels_train[train_index], labels_train[test_index]

            results = a.classify_w_different_classifiers(x_train, x_test, y_train, y_test)
            # results = a.evaluate_optimize_different_classifiers(x_train, x_test, y_train, y_test)
            for k, v in results.items():
                if v[-1] != 0:
                    prec = v[-2] / v[-1]
                else:
                    prec = np.nan
                if v[-3] != 0:
                    rec = v[-2] / v[-3]
                else:
                    rec = np.nan
                v.append(prec)  # precision
                v.append(rec)  # recall
                if k not in average_results_dict.keys():
                    average_results_dict[k] = np.add(np.zeros(9), np.array(v))
                else:
                    average_results_dict[k] = np.add(np.array(v), average_results_dict[k])

        # average values across folds:
        for k, v in average_results_dict.items():
            average_results_dict[k] = list(v[4:7])
            average_results_dict[k].extend(v[7:] / n_folds)
        for r, data in average_results_dict.items():
            res_curr_class.append([c, 'tfidf', 'mlrl1', r])
            res_curr_class[len(res_curr_class) - 1].extend(data)
        results_all_classes.extend(res_curr_class)
    rw.print_table(results_all_classes, 'results_classification_250headlines_by_class_w_xvalid_dom_transfer.csv',
                   headers=['Class', 'Method', 'ML', 'Classifier', 'Tot Correct',
                            '# Correctly Predicted', '# Guesses', 'Mean Precision', 'Mean Recall'])


def get_feature_names_from_tweets():
    # Input data parameters
    scipy.random.seed(0)
    gt_file_path = './data/tweets/NEW_emotions_tweets_no_ids_my_format.csv'
    doc_file_path = gt_file_path

    # Read input data
    documents_gt = rw.read_tsv_file(doc_file_path, headers=True, encoding='utf-8')
    documents_text = [d[0] for d in documents_gt]
    labels = [d[1:] for d in documents_gt]
    for i in range(len(labels)):
        for j in range(len(labels[i])):
            labels[i][j] = int(labels[i][j])
    labels = np.array(labels)
    p = {'documents': documents_text, 'size': None, 'option': 'tfidf'}
    print('indexing documents')
    docs_embeddings, feature_names = dec.get_document_representation(**p)
    docs_embeddings = np.array(docs_embeddings)
    feature_names = np.array(feature_names)
    labels = np.array([float(np.argmax(e)) for e in labels], dtype=np.float64)
    print('test size = 0.3, balanced, parallel')
    x_train, x_test, y_train, y_test = train_test_split(docs_embeddings, labels, test_size=0.3, random_state=42,
                                                        stratify=labels)
    print('training mlrl1 model')
    cvfit = cvglmnet(x=x_train.copy(), y=y_train.copy(), family='multinomial', mtype='grouped', parallel=True)
    print('model trained!')
    rw.save_model(cvfit, './models/model_mlrl1')
    cvfit = rw.load_model('./models/model_mlrl1')
    coeffs = cvglmnetCoef(cvfit, s='lambda_min')
    features_for_each_class = []
    for coeff_list in coeffs:
        coeff_list = coeff_list[1:]
        print('features list size: ' + str(len(coeff_list)))
        assert len(coeff_list) == len(feature_names)
        if len(np.nonzero(coeff_list)[0]) > 0:
            features_for_each_class.append(feature_names[np.nonzero(coeff_list)[0]])
        else:
            features_for_each_class.append(feature_names)
    return features_for_each_class


if __name__ == '__main__':
    run()
