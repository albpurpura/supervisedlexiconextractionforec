import os
import nlp
from gensim.models import Word2Vec
import numpy as np


def get_docs_embeddings(documents, model, size):
    docs_embs = []
    zero_docs_embs = 0
    for d in documents:
        de = np.zeros(size)
        tok = nlp.tokenize_quick(d)
        cnt = 0
        for t in tok:
            if t in model.wv.vocab:
                de = np.add(model.wv[t], de)
                cnt += 1
        if cnt > 0:
            de = np.divide(de, cnt)
        else:
            zero_docs_embs += 1
        docs_embs.append(de)
    if zero_docs_embs > 0:
        print(
            'There are ' + str(zero_docs_embs) + ' documents out of ' + str(len(documents)) + ' with null embeddings.')
    return docs_embs


def train_w2v_model(docs, size, model_dest='/Users/albertopurpura/PycharmProjects/ML4DC/w2vmodel'):
    class SentencesProvider(object):
        def __init__(self, dirname):
            self.dirname = dirname

        def __iter__(self):
            for d in docs:
                yield nlp.tokenize_quick(d)
            """
            for fname in os.listdir(self.dirname):
                for line in open(os.path.join(self.dirname, fname)):
                    # yield line.split()
                    # yield splitter(line)
                    yield nlp.tokenize_quick(line)
            """

    # WORD2VEC
    sentences = SentencesProvider(docs)  # a memory-friendly iterator

    ## word2vec on multi-words tokens could be useful in principle, but takes too long:
    # bigram_transformer = Phrases(sentences)
    # trigram_transformer = Phrases(bigram_transformer[sentences])
    # trigram_sentences=trigram_transformer[sentences]
    ## corpus = LineSentence(fname)

    # Gensim w2v syntax
    # class gensim.models.word2vec.Word2Vec(sentences=None, size=100, alpha=0.025, window=5, min_count=5, max_vocab_size=None, sample=0.001, seed=1, workers=3, min_alpha=0.0001, sg=0, hs=0, negative=5, cbow_mean=1, hashfxn=<built-in function hash>, iter=5, null_word=0, trim_rule=None, sorted_vocab=1, batch_words=10000):
    if os.path.exists(model_dest):
        model = Word2Vec.load(model_dest)  # KeyedVectors.load_word2vec_format(model_dest, encoding='utf-8')
    else:
        # we use skipgram (scales better with huge corpora)
        print('Training W2V model...')
        model = Word2Vec(sentences, size=size, min_count=10, window=5, workers=6, sg=1, negative=5, seed=42)
        print('Saving model...')
        model.save(model_dest)
    return model
