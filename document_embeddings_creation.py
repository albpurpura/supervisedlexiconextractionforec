import nlp
import tfidf
import numpy as np
import w2v
from gensim.models import Word2Vec


def get_document_representation(documents, size, option, model=None):
    if option == 'p2v':
        return get_de_p2v(documents, size)
    elif option == 'tfidf':
        return get_de_tfidf(documents, size, model)
    elif option == 'pl2':
        return get_de_pl2(documents, size)
    elif option == 'bm25':
        return get_de_bm25(documents, size)
    elif option == 'dlm':
        return get_de_dlm(documents, size)
    elif option == 'w2v':
        return get_de_w2v(documents, size, model)
    elif option == 'ftt':
        return get_de_w2v(documents, size, model)
    else:
        return None


def get_de_w2v(documents, size, model):
    de = w2v.get_docs_embeddings(documents, model, size)
    return de, range(0, len(de))


def get_de_tfidf(documents, size, vocabulary=None):
    return tfidf.get_tfidf_matrix_features_list_unsupervised(documents, size, vocabulary)


def get_collection_stats(docs):
    n_docs = len(docs)
    doc_lengths = []
    lexicon = []
    for i in range(len(docs)):
        tokenized = nlp.tokenize_quick(docs[i])
        lexicon.extend(tokenized)
        doc_lengths.append(len(tokenized))
    n_tokens = np.sum(doc_lengths)
    avg_doc_len = np.average(doc_lengths)
    lexicon = set(lexicon)
    lexicon_size = lexicon.__len__()
    return avg_doc_len, n_docs, n_tokens, lexicon_size
