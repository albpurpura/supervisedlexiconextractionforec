import os
import string
import nltk
import logging
from nltk.tokenize import sent_tokenize
from gensim.models import KeyedVectors, Word2Vec
from nltk.stem.porter import PorterStemmer
from pyspark import SparkContext, SparkConf
from nltk.corpus import stopwords
import re
import emot

__author__ = 'Alberto Purpura'
__copyright__ = 'Copyright 2018, University of Padova'
__credits__ = ['Alberto Purpura', 'Chiara Masiero', 'Gian Antonio Susto']
"""
Copyright 2018 University of Padua, Italy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

OS_PYTHON = '/anaconda3/bin/python3.6'


############################################################################
# W2V
def train_w2v_model(docs):
    """
    Train w2v model
    :param docs: the documents to use for the training.
    :return: the trained word2vec model.
    """
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    processed_docs = []
    for d in docs:
        processed_docs.append(tokenize(d))
    model = Word2Vec(processed_docs, min_count=1, size=300, workers=1, iter=10, sg=0, hashfxn=hash)
    return model


def save_w2v_model(model_path, model):
    """
    Saves a w2v model to file
    :param model_path: the path of the model to save
    :param model: the model to save
    :return:
    """
    model.wv.save_word2vec_format(model_path)
    return


def load_w2v_model(model_path):
    """
    Loads a w2v model from file
    :param model_path: file of the saved model to load.
    :return: the loaded model from the source file.
    """
    return KeyedVectors.load_word2vec_format(model_path)


def hash(astring):
    """
    A hash function to use in w2v model training phase.
    :param astring: the string to be hashed.
    :return: the hash of the string.
    """
    return ord(astring[0])


def extend_set_w2v(w2v_model, seed, max_words_to_add):
    """
    Extends a list of terms adding terms from a w2v model.
    :param w2v_model: the model to use to extend the seed list
    :param seed: the list of terms to add
    :param max_words_to_add: max number of terms to add to the list of words from the w2v model.
    :return: the extended version of the seed list.
    """
    new_set = []
    for w in seed:
        new_set.append(w)
        if w in w2v_model.wv.vocab:
            similar = w2v_model.wv.most_similar(w, topn=max_words_to_add)
            for e in similar:
                new_set.append(str.replace(e[0], '_', ' '))
    new_set = list(set(new_set))
    return new_set


############################################################################

def split_documents_in_sentences(documents):
    """
    Splits a list of documents in a list of sentences by splitting each document into sentences.
    :param documents: the documents to be splitted in sentences.
    :return: the list of sentences in the documents and an index of the indices of the docs to which each sentence in the previous list belongs.
    """
    sent_tokenize_list = []
    sentence_index = []
    for i in range(0, len(documents)):
        text = documents[i]
        text = add_spaces_after_punct_in_string(text)
        sentences = sent_tokenize(text)
        filtered_sents = []
        for s in sentences:
            if len(s) > 1:
                filtered_sents.append(s)
        sentences = filtered_sents
        if len(sentences) >= 1:
            sent_tokenize_list.extend(sentences)
            for k in range(0, len(sentences)):
                sentence_index.append(i)
    return sent_tokenize_list, sentence_index


def pre_process_document(doc, stemming=None):
    """
    Applies a series of pre-processing steps to a document.
    1. Removes stop words
    2. Removes short words (len(word) < 3)
    3. Removes numbers
    4. Removes puntuation
    5. Symbols (regex='[^\w]')
    :param doc: the document to process
    :param stemming: boolean value that indicates wether to apply stemming to the document or not.
    :return: the processed document in the form of a string.
    """
    doc = doc.lower()
    doc = doc.replace(r'.', ' ')
    if stemming:
        tok = tokenize(doc)
        filtered = remove_sw_shortwords_numbers_punct(tok)
        filtered = remove_symbols_from_token_list(filtered)
        for i in range(len(filtered)):
            filtered[i] = stem(filtered[i])
    else:
        tokenized = tokenize(doc)
        filtered = remove_sw_shortwords_numbers_punct(tokenized)
        filtered = remove_symbols_from_token_list(filtered)

    return untokenize(filtered)


def pre_process_documents(documents, stemming):
    """
    Applies the pre-processing steps from pre_process_document to a list of documents.
    :param documents: a list of strings = documents.
    :param stemming: boolean value that indicates wether to apply stemming to the documents or not.
    :return:
    """
    os.environ["PYSPARK_PYTHON"] = OS_PYTHON
    conf = SparkConf().setMaster("local[6]").set("spark.executor.memory", "6g").set("spark.driver.memory", "2g")
    sc = SparkContext(conf=conf)
    rdd = sc.parallelize(documents)
    processed = rdd.map(lambda doc: pre_process_document(doc, stemming)).collect()
    sc.stop()
    return processed


def untokenize(doc):
    """
    Transforms a list of tokens to a single string.
    :param doc: a list of strings
    :return: a string composed by the concatenation of the items in the "doc" list.
    """
    retval = ''
    for w in doc:
        retval = retval + ' ' + w
    return retval.lstrip(' ')


def tokenize(text):
    """
    Tokenizes a text.
    :param text: the text to be tokenized = a string.
    :return: a list of the tokens in the text.
    """
    text = add_spaces_after_punct_in_string(text)
    tokens = nltk.word_tokenize(text)
    tokens = remove_symbols_from_token_list(tokens)
    return tokens


def tokenize_with_stemming(text):
    """
    Tokenize a text and then returns the list of the stems of each token.
    :param text: the text to be tokenized.
    :return: the list of stemmed tokens in the text.
    """
    text = add_spaces_after_punct_in_string(text)
    tokens = tokenize(text)
    stems = []
    for item in tokens:
        stems.append(PorterStemmer().stem(item))
    return stems


def tokenize_quick(text):
    """
    Tokenizes a text removing first all punctuation characters and then splittin the text on space.
    :param text:
    :return: the list of token in the text
    """
    translator = re.compile('[%s]' % re.escape(string.punctuation))  # will be used to remove punctuation
    line = translator.sub(' ', text)  # replace punctuation with space
    line_non_ascii = re.sub(r'[^\x00-\x7f]', r'', line)  # removes all non ascii chars
    vals = emot.emoji(line)
    tokens = line_non_ascii.replace('  ', ' ').split()
    # if len(vals) > 1:
    #     found_emojis = vals['value']
    #     tokens.extend(found_emojis)  # collateral effect of moving emojis to the end of the text
    tokens = [t for t in tokens if not t.isdigit()]
    return tokens


def add_spaces_after_punct_in_string(s):
    return s.translate(str.maketrans({key: "{0} ".format(key) for key in string.punctuation})).replace('  ', ' ')


def remove_sw_shortwords_numbers_punct(sentence, lang='english'):
    # does what the name says, the sentence is a tokenized string = a list of words.
    filtered_words = []
    sw = set(nltk.corpus.stopwords.words(lang))
    exclude = set(string.punctuation)
    exclude.add('...')
    exclude.add('..')
    exclude.update([r'.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', '`', ' '])
    for w in sentence:
        w = w.lower()
        if (w not in sw) and (w not in exclude) \
                and ("'" not in w) \
                and (r'.' not in w) and (len(w) >= 3):  # and (not w.isdigit())
            filtered_words.append(w)
    return filtered_words


def remove_sw_punct_from_list(word_list):
    filtered_words = []
    sw = set(nltk.corpus.stopwords.words('english'))
    exclude = set(string.punctuation)
    exclude.add('...')
    exclude.add('..')
    exclude.update([r'.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}', '`'])
    for w in word_list:
        if (w not in sw) and (w not in exclude) \
                and ("'" not in w) \
                and (r'.' not in w):
            filtered_words.append(w)
    return filtered_words


def remove_symbols_from_token(t):
    return nltk.re.sub(r'[^\w]', ' ', t)


def remove_symbols_from_token_list(tokens):
    for i in range(len(tokens)):
        tokens[i] = remove_symbols_from_token(tokens[i])
    return tokens


def get_stemmed_list_of_terms_in_text(text, lang='en'):
    res = []
    if lang == 'en':
        terms = tokenize(text)
        for t in terms:
            stm = PorterStemmer().stem(t)
            res.append(stm)
        return res
    return text


def stem(text, lang='en'):
    if lang == 'en':
        res = get_stemmed_list_of_terms_in_text(text, lang)
        return ' '.join(res).strip()
    return text


def pre_process_document_keep_emojis(doc, stemming=None, lang='english'):
    """
        Applies a series of pre-processing steps to a document.
        1. Removes stop words
        2. Removes numbers
        3. Removes punctuation
        :param doc: the document to process
        :param stemming: boolean value that indicates wether to apply stemming to the document or not.
        :return: the processed document in the form of a string.
        """
    doc = doc.lower()
    doc = doc.replace(r'.', ' ')
    tok = tokenize_quick(doc)  # removes punctuation
    tok = [t for t in tok if t not in stopwords.words(lang) and (not t.isdigit())]  # removes digits and stopwords
    if stemming:
        for i in range(len(tok)):
            tok[i] = stem(tok[i], lang=lang)
    return untokenize(tok)


def pre_process_documents_keep_emojis(documents, stemming, lang='english'):
    """
    Applies the pre-processing steps from pre_process_document to a list of documents.
    :param documents: a list of strings = documents.
    :param stemming: boolean value that indicates wether to apply stemming to the documents or not.
    :return:
    """
    os.environ["PYSPARK_PYTHON"] = OS_PYTHON
    conf = SparkConf().setMaster("local[6]").set("spark.executor.memory", "8g").set("spark.driver.memory", "8g")
    sc = SparkContext(conf=conf)
    rdd = sc.parallelize(documents)
    processed = rdd.map(lambda doc: pre_process_document_keep_emojis(doc, stemming, lang)).collect()
    sc.stop()
    return processed
